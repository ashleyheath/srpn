#!/usr/bin/env python
# A reverse polish notation calculator

from sys import stdin, stdout, stderr, exit
from string import digits
from operators import *

class SrpnRandom:
    """Creates a class to mimic the srpn random number generator."""
    # Numbers taken from the original srpn random number generator
    randomList = [1562469902, 1039845534, 2025653534, 739593874, 994290584, 1198075102, 605335584, 563009619, 1076425455, 1979353639, 1481705266, 416282717, 1502074844,
                  339011283, 1656724019, 75412011, 296441807, 1150973001, 1935872936, 378814183, 1318686473, 2034028701, 1310947874, 2095686658, 1548890542, 987301502,
                  543737933, 1987654433, 111092794, 1359561819, 1253077815, 1673562696, 251923705, 1131247701, 265672923, 1246214289, 181839156, 871008507, 1809223908,
                  1258264611, 702878498, 1143445526, 1674547328, 57469695, 1482456809, 1183787700, 132881706, 1778898617, 187277053, 2068754642, 10229152, 1505963526,
                  1955299696, 1321177026, 1454166537, 1356706590, 160994880, 1997904470, 1196877376, 272087674, 1209982641, 302471543, 1945650371, 1461906347,
                  1433719244, 63839646, 560636988, 1615558400, 934848153, 222377249, 726339364,1637726652, 1365822775, 253403044, 1695196347, 700795937, 1437190744,
                  1828078053,332210906, 1624467798, 1749349048, 342440058, 982947676, 1557165096, 1663617085, 289630565, 766388038, 1824611965, 140051388, 1963265414,
                  2096699640, 1350034029, 118253309, 1894866363, 664456728, 1551972554, 1958706009, 1225093717, 1020047306, 746070514, 1447470966, 1746386670, 236313518,
                  665810093, 1999789715, 1931509865, 1366606030, 1289496811, 1612104271, 1698816936, 766480961, 1213969671, 2041256995, 1749428638, 623651119, 1557390432,
                  2039059203, 1390039157, 1234518749, 31626943, 1205820924, 1183734741, 1381660973, 1324074233, 931117456, 2046117701, 728563139, 742339817, 1123727770,
                  1748610446, 1488410332, 423715088, 1347513468, 1724723850, 1089525182, 1199819535, 1508750068, 308647564, 341832699, 973370691, 2007464501, 1108313660,
                  39856714, 1901237848, 710258650, 663507833, 1311144632, 601834206, 2053546990, 398179733, 633461149, 1111884266, 1581914475, 2015122122, 288474852,
                  365548283, 1913756176, 1017037991, 1107888101, 890000298, 618164789, 448814785, 1313715387, 1965678258, 26054987, 255756921, 1018014145, 1534805055,
                  564404485, 1359846844, 360692098, 424385338, 320676857, 400548812, 178139538, 1030935507, 1064056645, 1489284170, 1632769713, 970119988, 1887463904,
                  118747215, 2082004254, 1321894731, 2133869337, 222995458, 1687443014, 1900141865, 1240033450, 647847467, 642658516, 1858198239, 1096662252, 1956373903,
                  1676392849, 1122717240, 64647176, 546923347, 510038647, 629051661, 1906770191, 870730746, 1053437000, 79963400, 1271279558, 1231576538, 1110898908,
                  187852556, 573377061, 596184973, 1157972544, 313357317, 714932188, 1092493150, 1635252048, 701317878, 1315488609, 1175211414, 453976095, 408038411,
                  1823058882, 1096634611, 118753002, 772237486, 905524866, 1795145852, 1894954726, 970172042, 194585551, 257509726, 1599223704, 2101355742, 1128240472,
                  505177056, 33835495, 252036382, 1736753594, 1144734403, 439888938, 162647007, 1740919376, 1597861482, 476004324, 308367917, 542870985, 2111256372,
                  1009685795, 1858359594, 1138984139, 1463661890, 118914357, 814559373, 412812854, 237667359, 1586796859, 1318337720, 2032813211, 1334267938, 141026115,
                  79915114, 1591777664, 1740249819, 33787209, 572534488, 97943227, 67622704, 824570870, 1834696821, 1212357107, 1264459809, 1997343829, 805792835, 714837643,
                  325864505, 1114160752, 1257708628, 289637230, 2123846547, 968584574, 1428621369, 1440024790, 1087498931, 95697094,1852837644, 1325166291, 1682493953,
                  1023691716, 1210495854, 869278243, 1164717831, 1290410969, 313572259, 757484002, 1324198178, 886106747, 855427229, 1391820882, 1710677618]
    randomIndex = 0 # If we reach the end of the list this allows us to avoid an index error by returning to the start of the list
    def srpn_random(self):
        """Return a value based on the output of 'r' for the original srpn program."""
        if self.randomIndex < len(self.randomList):
            self.randomIndex += 1
            return self.randomList[self.randomIndex-1]
        else:
            self.randomIndex = 0
            return self.randomList[len(self.randomList)-1]
    
class MyStack:
    """Creates a stack object to store the operands entered into the program."""
    maxSize = 23 # The stack can contain 23 numbers at most
    stack = []
    def push(self, n):
        """Add the argument provided to the top of the stack.
        Return False if adding n would push the size of the stack above maxSize"""
        if len(self.stack) == self.maxSize:
            return False
        self.stack.append(n)
        return True
    def pop(self):
        """Return the item most recently placed on the stack."""
        if len(self.stack) < 1:
            return None
        t = self.stack[-1]
        del self.stack[-1]
        return t

def convert_user_input(inputString):
    """Split the input into operators and operands."""
    operatorsAndOperands = []
    decimalNumbers = digits
    currentNumber = ''
    currentMultiplyer = 1
    
    def end_is_next():
        """Returns true if the end of the input string will be reached on the next loop."""
        if len(inputString) == 1:
            return True
        else:
            return False
        
    while(len(inputString) > 0):
        # Comment => Ignore the rest of the line.
        if inputString[0] == '#':
            inputString = ''
        # Whitespace => Do nothing.
        elif inputString[0] == ' ':
            pass
        # Minus Sign => If before a number change currentMultiplyer, else treat as an operator.
        elif inputString[0] == '-':
            if not end_is_next():
                if inputString[1] in decimalNumbers:
                    currentMultiplyer = -1
                else:
                    operatorsAndOperands.append(inputString[0])
            else:
                operatorsAndOperands.append(inputString[0])
        # Numbers => Append to the end of the sting currentNumber.
        # If this is the last digit, convert it to a decimal integer and add it to operatorsAndOperands.
        elif inputString[0] in decimalNumbers:
            currentNumber += inputString[0]
            if end_is_next() or inputString[1] not in decimalNumbers:
                num = 0
                # If it is an octal number
                if currentNumber[0] == '0':
                    for i in range(len(currentNumber)-1):
                        num += int(currentNumber[-(i+1)])*8**i
                # If it is a decimal number
                else:
                    for i in range(len(currentNumber)):
                        num += int(currentNumber[-(i+1)])*10**i
                n = sat(num*currentMultiplyer)# Test for saturation
                operatorsAndOperands.append(n)
                currentNumber = ''# Reset the number string
                currentMultiplyer = 1# Reset the multiplyer
        # Everything Else
        else:
            operatorsAndOperands.append(inputString[0])
        inputString = inputString[1:]
    return operatorsAndOperands

def main():
    """The main loop of the srpn program."""
    rand = SrpnRandom()
    stack = MyStack()
    while(True):
        userInput = stdin.readline()
        # If the user inputs nothing then exit. Used to handle EOF when data is piped into the program.
        if userInput == '':
            exit()
        userInput = userInput.strip()# Removes new line to prevent an uncrecognised input error
        operatorsAndOperands = convert_user_input(userInput)
        for uin in operatorsAndOperands:
            # Numbers are added to the stack
            if type(uin) == int:
                if stack.push(uin) != True:
                    stderr.write('Stack overflow.\n')
            # Prints all the numbers in the stack, starting with the least recent
            elif uin == 'd':
                for i in stack.stack:
                    stdout.write(str(i)+'\n')
            # Prints the most recent number in the stack
            elif uin == '=':
                x = stack.pop()
                if x != None:
                    stdout.write(str(x)+'\n')
                    stack.push(x)
                else:
                    stderr.write('Stack empty.\n')
            # Prints the next number in the random list
            elif uin == 'r':
                if stack.push(rand.srpn_random()) != True:
                    stderr.write('Stack overflow.\n')
            elif uin == '+':
                addition(stack)
            elif uin == '-':
                subtraction(stack)
            elif uin == '*':
                multiplication(stack)
            elif uin == '/':
                division(stack)
            elif uin == '%':
                modulus(stack)
            elif uin == '^':
                power(stack)
            else:
                stderr.write('Unrecognised operator or operand "%s".\n' %uin)

def srpn():
    """Called when the program is run. Executes the mainloop and handles exceptions."""
    try:
        main()
    except (SystemExit, EOFError):
        exit()
    except  KeyboardInterrupt:
        stdout.write('\n')
        exit()
    
if __name__ == '__main__':
    srpn()
