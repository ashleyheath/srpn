from sys import stderr

def stack_deficiency(num1, num2):
    """Return True if there are less than two numbers in a stack and then output an error."""
    if num1 == None or num2 == None:
        stderr.write('Stack underflow.\n')
        return True
    else:
        return False

def div_by_zero(n):
    """Return True if n == 0 and then output an error."""
    if n == 0:
        stderr.write('Divide by 0.\n')
        return True
    else:
        return False

def sat(n):
    """Check a number to see if it exceeds the bounds of numbers allowed by srpn."""
    if n >= 2147483647:
        return 2147483647
    elif n <= -2147483648:
        return -2147483648
    else:
        return n  

def get_two_operands(stack):
    """Return the top two items from a stack."""
    num1 = stack.pop()
    num2 = stack.pop()
    return num1, num2

def set_two_operands(topNum, bottomNum, stack):
    """Add two items to the top of a stack."""
    # When a stack does not have enough items to meet pop requests it will return None
    if bottomNum != None:
        # This prevents items that are not actually numbers being added to the stack
        stack.push(bottomNum)
    if topNum != None:
            stack.push(topNum)

def addition(stack):
    """Push to a stack the addition of the top two numbers from the stack."""
    num1, num2 = get_two_operands(stack)
    if not stack_deficiency(num1, num2):
        r = sat(num1 + num2)
        stack.push(r)
    else:
        set_two_operands(num1, num2, stack)

def subtraction(stack):
    """Push to a stack the subtraction of the top number on the stack from the one that comes after it."""
    num1, num2 = get_two_operands(stack)
    if not stack_deficiency(num1, num2):
        r = sat(num2 - num1)
        stack.push(r)
    else:
        set_two_operands(num1, num2, stack)

def multiplication(stack):
    """Push to a stack the multiplication of the top two numbers from the stack."""
    num1, num2 = get_two_operands(stack)
    if not stack_deficiency(num1, num2):
        r = sat(num1 * num2)
        stack.push(r)
    else:
        set_two_operands(num1, num2, stack)
        
def division(stack):
    """Push to a stack the division of the 2nd most from top number on the stack by the top one."""
    num1, num2 = get_two_operands(stack)
    if not stack_deficiency(num1, num2) and not div_by_zero(num1):
        num1sign = num1/abs(num1)
        num2sign = 1
        if num2 != 0:
            num2sign = num2/abs(num2)
        sign = num1sign*num2sign
        r = sat((abs(num2) / abs(num1))*sign) # because of t-single 08
        stack.push(r)
    else:
        set_two_operands(num1, num2, stack)

def modulus(stack):
    """Push to a stack the modulus of the 2nd from top number on the stack divided by the top one."""
    # For srpn modulus the sign of the first number is the sign of the resulting number
    # and the modulus operation acts as if it were operating on two unsigned numbers
    # and then applies the sign of the first to the result
    num1, num2 = get_two_operands(stack)
    if not stack_deficiency(num1, num2) and not div_by_zero(num1):
        num2sign = 1
        if num2!=0:
            num2sign = num2/abs(num2) # because of t-single 07
        r = sat((abs(num2) % abs(num1))*num2sign)
        stack.push(r)
    else:
        set_two_operands(num1, num2, stack)

def power(stack):
    """Push to s stack the 2nd from top number on the stack to the power of the top one."""
    num1, num2 = get_two_operands(stack)
    if not stack_deficiency(num1, num2):
        sign = 1
        # If the operand is less than 0
        if num2 < 0:
            # And the power is divisible by 2
            if num1%2 == 0:
                sign = 1
            else:
                sign = -1
        if num2 == 0:
            num1 = abs(num1)# 0 cannot be raised to a negative power
        num = (abs(num2) ** abs(num1))*sign
        r = sat(num)
        r = int(r)# because of t-multiple05 (+ve**-ve converts to float)
        stack.push(r)
    else:
        set_two_operands(num1, num2, stack)
